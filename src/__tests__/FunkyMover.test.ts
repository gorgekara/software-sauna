import { test, expect } from "vitest";
import FunkyMover from "../FunkyMover";
import {
  BasicExample,
  IntersectionShake,
  LettersOnTurnsShake,
  LettersOnSameLocation,
  ExtraBasic,
  FakeTurn,
  IgnoreStuffAfterEnd,
  NoEnd,
  NoStart,
  MultipleStarts,
  ForkInRoad,
  MultipleEnds,
  BrokenPath,
} from "../__mocks__/maps";

test("Basic path", () => {
  const basicShake = new FunkyMover(BasicExample);
  basicShake.startShaking();

  expect(basicShake.letters).toBe("ACB");
  expect(basicShake.path).toBe("@---A---+|C|+---+|+-B-x");
});

test("Extra Basic path", () => {
  const extraBasicShake = new FunkyMover(ExtraBasic);
  extraBasicShake.startShaking();

  expect(extraBasicShake.letters).toBe("AB");
  expect(extraBasicShake.path).toBe("@-+|+A-+|B+-x");
});

test("Intersections in the path", () => {
  const intersectionShake = new FunkyMover(IntersectionShake);
  intersectionShake.startShaking();

  expect(intersectionShake.letters).toBe("ABCD");
  expect(intersectionShake.path).toBe("@|A+---B--+|+--C-+|-||+---D--+|x");
});

test("Letters on turns", () => {
  const lettersOnTurnsShake = new FunkyMover(LettersOnTurnsShake);
  lettersOnTurnsShake.startShaking();

  expect(lettersOnTurnsShake.letters).toBe("ACB");
  expect(lettersOnTurnsShake.path).toBe("@---A---+|||C---+|+-B-x");
});

test("Don't collect letters on same location twice", () => {
  const lettersOnSameLocation = new FunkyMover(LettersOnSameLocation);
  lettersOnSameLocation.startShaking();

  expect(lettersOnSameLocation.letters).toBe("GOONIES");
  expect(lettersOnSameLocation.path).toBe("@-G-O-+|+-+|O||+-O-N-+|I-+|ES|x");
});

test("Fake turn", () => {
  const fakeTurn = new FunkyMover(FakeTurn);
  fakeTurn.startShaking();

  expect(fakeTurn.letters).toBe("AB");
  expect(fakeTurn.path).toBe("@-A-+-B-x");
});

test("Ignore stuff after end", () => {
  const ignoreStuffAfterEnd = new FunkyMover(IgnoreStuffAfterEnd);
  ignoreStuffAfterEnd.startShaking();

  expect(ignoreStuffAfterEnd.letters).toBe("AB");
  expect(ignoreStuffAfterEnd.path).toBe("@-A-+|+-B--x");
});

test("Throw error if no start found", () => {
  const noStart = new FunkyMover(NoStart);
  expect(() => noStart.startShaking()).toThrowError(
    /Please make sure to include one starting point/
  );
});

test("Throw error if no end found", () => {
  const noEnd = new FunkyMover(NoEnd);
  expect(() => noEnd.startShaking()).toThrowError(
    /Please make sure to include one end point/
  );
});

test("Throw error if multiple start points found", () => {
  const multipleStarts = new FunkyMover(MultipleStarts);
  expect(() => multipleStarts.startShaking()).toThrowError(
    /Please make sure to include one starting point/
  );
});

test("Throw error if multiple end points found", () => {
  const multipleEnds = new FunkyMover(MultipleEnds);
  expect(() => multipleEnds.startShaking()).toThrowError(
    /Please make sure to include one end point/
  );
});

test("Throw error if multiple end points found", () => {
  const forkInRoad = new FunkyMover(ForkInRoad);
  expect(() => forkInRoad.startShaking()).toThrowError(
    /Path has too many forks/
  );
});

test("Throw error if path is broken", () => {
  const brokenPath = new FunkyMover(BrokenPath);
  expect(() => brokenPath.startShaking()).toThrowError(/Path is broken/);
});

test("findPositions method should work properly", () => {
  const basicShake = new FunkyMover(BasicExample);
  const positions = basicShake.findPositions();

  expect(positions?.start?.row).toBe(0);
  expect(positions?.start?.col).toBe(0);
  expect(positions?.isStartValid).toBe(true);
  expect(positions?.isEndValid).toBe(true);
  expect(positions?.hasForkInPath).toBe(false);
});

test("findPositions method should detect fork", () => {
  const forkShake = new FunkyMover(ForkInRoad);
  const positions = forkShake.findPositions();

  expect(positions?.hasForkInPath).toBe(true);
});

test("findPositions method should detect multiple start points", () => {
  const multipleStarts = new FunkyMover(MultipleStarts);
  const positions = multipleStarts.findPositions();

  expect(positions?.isStartValid).toBe(false);
});

test("findPositions method should detect multiple end points", () => {
  const multipleEnds = new FunkyMover(MultipleEnds);
  const positions = multipleEnds.findPositions();

  expect(positions?.isEndValid).toBe(false);
});

test("addToPath method should append to path", () => {
  const basicShake = new FunkyMover(BasicExample);
  basicShake.addToPath("A");
  expect(basicShake.path).toBe("@A");
});

test("addLetter method should append to letters", () => {
  const basicShake = new FunkyMover(BasicExample);
  basicShake.addLetter("A", "a01");
  expect(basicShake.letters).toBe("A");
});

test("addLetter method should not append letters on same position", () => {
  const basicShake = new FunkyMover(BasicExample);
  basicShake.addLetter("A", "a01");
  basicShake.addLetter("B", "b02");
  basicShake.addLetter("A", "a01");
  expect(basicShake.letters).toBe("AB");
});

test("forkDetector method should not detect forks in basic example", () => {
  const basicShake = new FunkyMover(BasicExample);
  const hasForks = basicShake.forkDetector();
  expect(hasForks).toBe(false);
});

test("forkDetector method should detect forks in forks example", () => {
  const forkShake = new FunkyMover(ForkInRoad);
  const hasForks = forkShake.forkDetector();
  expect(hasForks).toBe(true);
});

test("determineNextMove should work", () => {
  const basicShake = new FunkyMover(BasicExample);
  const move1 = basicShake.determineNextMove({ row: 0, col: 0 }, []);
  expect(move1?.direction?.row).toBe(0);
  expect(move1?.direction?.col).toBe(1);
  expect(move1?.origin?.[0]).toBe(0);
  expect(move1?.origin?.[1]).toBe(1);

  const move2 = basicShake.determineNextMove({ row: 0, col: 4 }, [
    "01",
    "02",
    "03",
  ]);
  expect(move2?.direction?.row).toBe(0);
  expect(move2?.direction?.col).toBe(5);
  expect(move2?.origin?.[0]).toBe(0);
  expect(move2?.origin?.[1]).toBe(1);

  const move3 = basicShake.determineNextMove({ row: 0, col: 8 }, [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
  ]);
  expect(move3?.direction?.row).toBe(1);
  expect(move3?.direction?.col).toBe(8);
  expect(move3?.origin?.[0]).toBe(1);
  expect(move3?.origin?.[1]).toBe(0);

  const move4 = basicShake.determineNextMove({ row: 0, col: 7 }, [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
  ]);
  expect(move4?.direction?.row).toBe(0);
  expect(move4?.direction?.col).toBe(8);
  expect(move4?.origin?.[0]).toBe(0);
  expect(move4?.origin?.[1]).toBe(1);
});
