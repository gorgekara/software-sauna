type StepType = {
  row: number;
  col: number;
};

class FunkyMover {
  start: string = "@";
  end: string = "x";
  allowedMoves: string[] = ["-", "|", "+"];
  arrayToTest: Array<string[]>;
  path: string = "@";
  letters: string = "";
  insertedLetters: string[] = [];
  directions: Array<number[]> = [
    [-1, 0], // up
    [1, 0], // down
    [0, -1], // left
    [0, +1], // right
  ];

  constructor(arrayToTest: Array<string[]>) {
    this.arrayToTest = arrayToTest;
  }

  findPositions() {
    if (!this.arrayToTest?.length) return;

    let start: StepType | undefined;
    let end: StepType | undefined;
    let startCount = 0;
    let endCount = 0;

    this.arrayToTest.forEach((row: string[], rowIndex: number) => {
      row.forEach((value: string, columnIndex: number) => {
        if (value === this.start) {
          start = { row: rowIndex, col: columnIndex };
          startCount += 1;
        }

        if (value === this.end) {
          end = { row: rowIndex, col: columnIndex };
          endCount += 1;
        }
      });
    });

    const hasForkInPath = this.forkDetector();

    const isStartValid = typeof start !== "undefined" && startCount === 1;
    const isEndValid = typeof end !== "undefined" && endCount === 1;

    return { start, end, isStartValid, isEndValid, hasForkInPath };
  }

  addToPath(value: string) {
    this.path += value;
  }

  addLetter(value: string, letterKey: string) {
    const lettersReg = new RegExp(/[A-Z]/);
    if (this.insertedLetters.includes(letterKey) || !lettersReg.test(value))
      return;

    this.letters += value;
    this.insertedLetters.push(letterKey);
  }

  forkDetector() {
    const forker: number[] = [];

    this.arrayToTest?.forEach((row, rowIndex: number) => {
      row?.forEach((value, colIndex: number) => {
        if (value === "+") {
          let forkCounter = 0;
          const lettersReg = new RegExp(/[A-Z]/);

          this.directions.forEach((possibleDirection) => {
            const row = rowIndex + possibleDirection[0];
            const col = colIndex + possibleDirection[1];

            const value = this.arrayToTest?.[row]?.[col];

            if (
              this.allowedMoves.includes(value) ||
              lettersReg.test(value) ||
              value === this.end
            ) {
              forkCounter += 1;
            }
          });

          forker.push(forkCounter);
        }
      });
    });

    return forker.some((i) => i >= 3);
  }

  determineNextMove(
    currentStep: StepType,
    passPositions: string[],
    oldDirection?: any
  ) {
    let direction: any;
    let origin: any;

    const lettersReg = new RegExp(/[A-Z]/);

    this.directions.forEach((possibleDirection) => {
      const row = currentStep.row + possibleDirection[0];
      const col = currentStep.col + possibleDirection[1];
      const value = this.arrayToTest?.[row]?.[col];

      if (
        (this.allowedMoves.includes(value) ||
          lettersReg.test(value) ||
          value === this.end) &&
        !passPositions.includes(`${row}${col}`)
      ) {
        direction = { row, col };
        origin = possibleDirection;
      }
    });

    if (typeof direction === "undefined" && oldDirection) {
      const row = currentStep.row + oldDirection[0];
      const col = currentStep.col + oldDirection[1];

      const value = this.arrayToTest?.[row]?.[col];

      if (this.allowedMoves.includes(value) || lettersReg.test(value)) {
        direction = { row: row, col: col };
        origin = oldDirection;
      }
    }

    if (typeof direction !== "undefined") {
      const value = this.arrayToTest?.[direction.row]?.[direction.col];
      const letterKey = `${value}${direction.row}${direction.col}`;

      if (value === this.end) {
        direction = undefined;
      }

      this.addToPath(value);
      this.addLetter(value, letterKey);
    }

    return { direction, origin };
  }

  printArray() {
    this.arrayToTest.forEach((i) => console.log(...i));
  }

  printInfo() {
    console.log("\n");
    console.log("Letters:", this.letters);
    console.log("Path as characters:", this.path);
    console.log("\n");
  }

  startShaking() {
    const { start, isStartValid, isEndValid, hasForkInPath }: any =
      this.findPositions();

    if (!isStartValid) {
      this.printArray();
      throw Error("👆 Please make sure to include one starting point '@'");
    }

    if (!isEndValid) {
      this.printArray();
      throw Error("👆 Please make sure to include one end point 'x'");
    }

    if (hasForkInPath) {
      this.printArray();
      throw Error("👆 Path has too many forks!");
    }

    let currentStep = start;

    const passPositions: string[] = [];

    const { direction, origin } = this.determineNextMove(
      currentStep,
      passPositions
    );

    const moveToNextStep = (step: StepType | undefined, oldDirection: any) => {
      if (typeof step === "undefined") return;
      currentStep = step;

      const { direction, origin } = this.determineNextMove(
        currentStep,
        passPositions,
        oldDirection
      );

      if (
        typeof direction === "undefined" &&
        this.path.charAt(this.path?.length - 1) !== "x"
      ) {
        this.printArray();
        throw Error("👆 Path is broken!");
      }

      if (typeof direction === "undefined") {
        this.printArray();
        this.printInfo();
      }

      passPositions.push(`${step.row}${step.col}`);
      moveToNextStep(direction, origin);
    };

    moveToNextStep(direction, origin);
  }
}

export default FunkyMover;
