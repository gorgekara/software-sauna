import "./style.css";

import {
  BasicExample,
  MultipleEnds,
  ForkInRoad,
  BrokenPath,
  LettersOnTurnsShake,
  IntersectionShake,
  ExtraBasic,
  LettersOnSameLocation,
  FakeTurn,
  IgnoreStuffAfterEnd,
  NoEnd,
  MultipleStarts,
} from "./__mocks__/maps";
import FunkyMover from "./FunkyMover";

new FunkyMover(BasicExample).startShaking();
new FunkyMover(LettersOnTurnsShake).startShaking();
new FunkyMover(IntersectionShake).startShaking();
new FunkyMover(ExtraBasic).startShaking();
new FunkyMover(LettersOnSameLocation).startShaking();
new FunkyMover(FakeTurn).startShaking();
new FunkyMover(IgnoreStuffAfterEnd).startShaking();
new FunkyMover(NoEnd).startShaking();
new FunkyMover(MultipleStarts).startShaking();
new FunkyMover(MultipleEnds).startShaking();
new FunkyMover(ForkInRoad).startShaking();
new FunkyMover(BrokenPath).startShaking();

document.querySelector<HTMLDivElement>("#app")!.innerHTML = `
  <div>
    <h1>Funky Mover</h1>
    <h4>by Gjorge Karakabakov</h4>
    <br />
    <p>Check out the Development Console for the output</p>
  </div>
`;
