# Software Sauna Code Challenge

Hi 👋 - this is one solution for the [Software Sauna Code Challenge](https://github.com/softwaresauna/code-challenge).

Author: [Gjorge Karakabakov](https://karakabakov.com)

## Some Info

The main file that contains all the logic is `./src/FunkyMover.ts`. This file contains the class that manages the 2 dimensional array and determins the path.
Also, I've added some mock 2 dimensional arrays in the `./src/__mocks__` folder. All tests can be found in the `./src/__tests__` folder. Tests validate the mock 2 dimensional arrays as a whole and individual methods within the `FunkyMover` class.

## Setup

To install all dependencies please run:

```
npm install
```

## Run Project

To run the project in dev mode:

```
npm run dev
```

This will start the dev server on a specific port. Usually the port number is outputed in the terminal.

## Run Tests

To run all project tests:

```
npm run test
```

This will run all Vitetest tests.
